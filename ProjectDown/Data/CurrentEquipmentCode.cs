﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class CurrentEquipment : MonoBehaviour
    {
        #region properties
        public GameObject headSlot { get; set; }
        public GameObject shoulderSlot { get; set; }
        public GameObject neckSlot { get; set; }
        public GameObject rightEarSlot { get; set; }
        public GameObject leftEarSlot { get; set; }
        public GameObject torsoSlot { get; set; }
        public GameObject rightForearmSlot { get; set; }
        public GameObject leftForearmSlot { get; set; }
        public GameObject leftHandSlot { get; set; }
        public GameObject rightHandSlot { get; set; }
        public GameObject legsSlot { get; set; }
        public GameObject feetSlot { get; set; }

        private List<GameObject> inventory;
        public List<GameObject> GetInventory
        {
            get
            {
                if (inventory == null)
                {
                    inventory = new List<GameObject>();
                }
                return inventory;
            }
        }
        public void SetInventory(List<GameObject> newInventory)
        {
            inventory = newInventory;
        }
        #endregion
        #region Behaviors
        public void AddToInventory(GameObject newItem)
        {
            if(inventory != null)
            {
                inventory.Add(newItem);
            }
            else
            {
                inventory = new List<GameObject>();
                inventory.Add(newItem);
            }
        }
        #endregion
    }
}
