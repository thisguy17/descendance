﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public CurrentEquipment currentWeapon;

	// Use this for initialization
	void Start () {
        //set current weapon
        currentWeapon = GetComponentInParent<CurrentEquipment>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
