﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToMove : MonoBehaviour {

    Vector3 v3_target;
    Vector3 v3_previousPosition;
    float fAngleFromDown;

    Animator animator;

    public float speed;

    //states
    const int STATE_IDLE_DOWN = 0;
    const int STATE_IDLE_DOWN_LEFT = 1;
    const int STATE_IDLE_LEFT = 2;
    const int STATE_IDLE_UP_LEFT = 3;
    const int STATE_IDLE_UP = 4;
    const int STATE_IDLE_UP_RIGHT = 5;
    const int STATE_IDLE_RIGHT = 6;
    const int STATE_IDLE_DOWN_RIGHT = 7;

    // Use this for initialization
    void Start () {
        v3_target = transform.position;
        v3_previousPosition = transform.position;
        animator = GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonUp(1))
        {
            //right click

            v3_previousPosition = transform.position;
            v3_target = Input.mousePosition;

            //animate movement
            Animate();
        }

        //update movement
        MoveToLocation();
	}

    void MoveToLocation()
    {
        if(transform.position != v3_target)
        {
            transform.position = Vector2.MoveTowards(transform.position, v3_target, speed * Time.deltaTime);
        }
    }

    void Animate()
    {
        Vector3 movedir = (v3_previousPosition - v3_target).normalized;

        fAngleFromDown = Vector3.Angle(Vector3.down, movedir);

        //check for positive/negative
        var cross = Vector3.Cross(Vector3.down, movedir);
        if (cross.y < 0)
            fAngleFromDown = -fAngleFromDown;

        if (fAngleFromDown <= 22 && fAngleFromDown >= -22)
        {
            //down
            
        }
        else if(fAngleFromDown <= 67 && fAngleFromDown >= 23)
        {
            //down left
        }
        else if (fAngleFromDown <= 112 && fAngleFromDown >= 68)
        {
            //left
        }
        else if (fAngleFromDown <= 157 && fAngleFromDown >= 113)
        {
            //up left
        }
        else if (fAngleFromDown <= 180 && fAngleFromDown >= 158 || fAngleFromDown <= -158 && fAngleFromDown >= -180)
        {
            //up
        }
        else if (fAngleFromDown <= -23 && fAngleFromDown >= -67)
        {
            //up right
        }
        else if (fAngleFromDown <= -68 && fAngleFromDown >= -112)
        {
            //right
        }
        else
        {
            //down right
        }
    }

    enum CharacterForward
    {
        down = 0, downleft = 1, left = 2, upleft = 3, up = 4, upright = 5, right = 6, downright = 7
    }
}
