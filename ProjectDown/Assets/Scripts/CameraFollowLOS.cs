﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowLOS : MonoBehaviour {

    AnimationState state;
    Vector3 direction;
    Vector3 newCameraLocation;
    
    public float _spot = 3; //changes zoom (visible area) beyond a certain point; reveals fog otherwise
    public float smooth = 15f; //15 seems good; testing required for finalization

	// Use this for initialization
	void Start () {
        //modify listen and spot values to match character stats (get)
        state = transform.parent.GetComponent<AnimationState>();
        direction = state.movementDirection.normalized;
        newCameraLocation = direction * (_spot * 0.1f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //update skill numbers; _spot = CALCULATE SKILL


        direction = state.movementDirection.normalized;
        newCameraLocation = direction;
        transform.localPosition = Vector3.Lerp(transform.localPosition, -newCameraLocation, Time.deltaTime * smooth);
    }
}
