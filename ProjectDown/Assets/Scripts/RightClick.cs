﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightClick : MonoBehaviour
{
    Vector3 v3_target;
    Vector3 v3_previousPosition;
    Vector3 v3_movementDirection;
    Animator[] animators;
    bool isMoving = false;
    AnimationState.CharacterState previousCharacterState;

    public float speed;
    public AnimationState state;
    public bool isTargeting = false;

    // Use this for initialization
    void Start () {
        v3_target = transform.position;
        v3_previousPosition = transform.position;
        animators = GetComponentsInChildren<Animator>();
        state = GetComponent<AnimationState>();
        v3_movementDirection = new Vector3(0, -1, 0);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButton(1) || Input.GetMouseButtonUp(1) && !isTargeting)
        {
            //right click
            v3_previousPosition = transform.position;
            v3_target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButtonUp(1) && isTargeting)
        {

        }

        //update movement
        MoveToLocation();

        if(state._characterState != previousCharacterState)
        {
            //animate state
            foreach(var animator in animators)
            {
                animator.SetInteger("state", (int)state._characterState);
            }
            previousCharacterState = state._characterState;
        }
    }

    void MoveToLocation()
    {
        if(Math.Abs(v3_target.z) != 0)
        {
            v3_target.z = transform.position.z;
        }
        if(transform.position != v3_target)
        {
            isMoving = true;
            transform.position = Vector3.MoveTowards(transform.position, v3_target, speed * Time.deltaTime);
            v3_movementDirection = (v3_previousPosition - v3_target).normalized;
        }
        else if(isMoving)
        {
            isMoving = false;
        }

        if (isMoving)
        {
            bool isLeft = v3_target.y < 0;
        }

        //set animation state
        state.SetState(v3_movementDirection, isMoving, v3_target.x <= 0);
    }
}
