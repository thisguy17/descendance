﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationState : MonoBehaviour
{
    public CharacterState _characterState;
    public Vector3 movementDirection;

    float fAngleFromDown = 0;
    bool isLeftHemisphere = false;

    // Use this for initialization
    void Start () {
        _characterState = CharacterState.idown;
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void SetState(Vector3 movedir, bool isMoving, bool isLeft)
    {
        fAngleFromDown = Vector3.Angle(movedir, Vector3.up);
        isLeftHemisphere = isLeft;
        movementDirection = movedir;

        //check for left/right
        if (isLeft)
        {//left
            if (fAngleFromDown <= 22)
            {
                //format: i<direction> = idle, m<direction> = moving; clockwise from screen down
                //down
                if (isMoving)
                {
                    _characterState = CharacterState.mdown;
                }
                else
                {
                    _characterState = CharacterState.idown;
                }
            }
            else if (fAngleFromDown <= 67 && fAngleFromDown >= 23)
            {
                //down left
                if (isMoving)
                {
                    _characterState = CharacterState.mdownleft;
                }
                else
                {
                    _characterState = CharacterState.idownleft;
                }
            }
            else if (fAngleFromDown <= 112 && fAngleFromDown >= 68)
            {
                //left
                if (isMoving)
                {
                    _characterState = CharacterState.mleft;
                }
                else
                {
                    _characterState = CharacterState.ileft;
                }
            }
            else if (fAngleFromDown <= 157 && fAngleFromDown >= 113)
            {
                //up left
                if (isMoving)
                {
                    _characterState = CharacterState.mupleft;
                }
                else
                {
                    _characterState = CharacterState.iupleft;
                }
            }
            else
            {
                //up
                if (isMoving)
                {
                    _characterState = CharacterState.mup;
                }
                else
                {
                    _characterState = CharacterState.iup;
                }
            }
        }
        else
        {//right
            if(fAngleFromDown <= 22)
            {
                //down
                if (isMoving)
                {
                    _characterState = CharacterState.mdown;
                }
                else
                {
                    _characterState = CharacterState.idown;
                }
            }
            else if (fAngleFromDown >= 23 && fAngleFromDown <= 67)
            {
                //down right
                if (isMoving)
                {
                    _characterState = CharacterState.mdownright;
                }
                else
                {
                    _characterState = CharacterState.idownright;
                }
            }
            else if (fAngleFromDown >= 68 && fAngleFromDown <= 112)
            {
                //right
                if (isMoving)
                {
                    _characterState = CharacterState.mright;
                }
                else
                {
                    _characterState = CharacterState.iright;
                }
            }
            else if (fAngleFromDown >= 113 && fAngleFromDown <= 157)
            {
                //up right
                if (isMoving)
                {
                    _characterState = CharacterState.mupright;
                }
                else
                {
                    _characterState = CharacterState.iupright;
                }
            }
            else
            {
                //up
                if (isMoving)
                {
                    _characterState = CharacterState.mup;
                }
                else
                {
                    _characterState = CharacterState.iup;
                }
            }
        }
    }

    public enum CharacterState
    {
        idown,
        idownleft,
        ileft,
        iupleft,
        iup,
        iupright,
        iright,
        idownright,
        mdown,
        mdownleft,
        mleft,
        mupleft,
        mup,
        mupright,
        mright,
        mdownright
    }
}
